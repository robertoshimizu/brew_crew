# brew_crew

A new Flutter project.

## Getting Started

This project follows along the tutorial from [The Net Ninjas](https://www.youtube.com/watch?v=sfA3NWDBPZ4&list=PL4cUxeGkcC9j--TKIdkb3ISfRbJeJYQwC&index=1).

A few resources to get you started if this is your first Flutter project:

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

In VSCode, install Flutter and Dart modules.
Create a Firebase project and then a Android App. ApplicationID can be found in project's build.gradle located at the **app** folder.
Download the **google-services.json** file and move it to the app root folder. Follow the instructions from Firebase to add the **Firebase SDK** lines to the respective **build.gradle** files.

In **pubspec.yaml**, add the following packages, which can be found at https://pub.dev/

```
dependencies:
    firebase_auth: ^0.16.1
    cloud_firestore: ^0.13.6
```
